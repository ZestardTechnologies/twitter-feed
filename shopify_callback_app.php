<?php
require 'vendor/autoload.php';
use shopifymodel\shopify_api;


session_start(); //start a session

$db = new Mysqli("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_twitter_feed");

if($db->connect_errno){
  die('Connect Error: ' . $db->connect_errno);
}

$select_settings = $db->query("SELECT * FROM appsettings WHERE id = 1");
$app_settings = $select_settings->fetch_object();

if(!empty($_GET['shop'])){ //check if the shop name is passed in the URL
  $shop = $_GET['shop']; //shop-name.myshopify.com    

  $select_store = $db->query("SELECT store_name FROM usersettings WHERE store_name = '".$shop."'"); //check if the store exists
  if($select_store->num_rows > 0){      
      if(shopify_api\is_valid_request($_GET, $app_settings->shared_secret)){ //check if its a valid request from Shopify                           
          
          $_SESSION['shopify_signature'] = $_GET['signature'];          
          $_SESSION['shop'] = $shop;
          $_SESSION['api_key'] = $app_settings->api_key;                    
          header('Location: https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/'); //redirect to the admin page
      }
  }
  else {     
          
      //convert the permissions to an array
      $permissions = json_decode($app_settings->permissions, true);      
      //get the permission url
      $permission_url = shopify_api\permission_url(
          $_GET['shop'], $app_settings->api_key, $permissions
      );

      $permission_url .= '&scope=read_script_tags,write_script_tags&redirect_uri=' . $app_settings->redirect_url;      
      
      header('Location: ' . $permission_url); //redirect to the permission url
  }
}


?>