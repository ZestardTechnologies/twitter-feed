<div id="wrap">
    <div class="col-md-8 col-xs-12">
        <h3 class="page-title">Manage Embedded Single Tweet</h3>

        <?php echo form_open('Settings/single_embedded_tweet_add', array('name' => 'single_tweet', 'class' => 'custom-form-design', 'style' => 'border: 1px solid #ccc;padding: 20px;')); ?>
        <?php $getStoreCollection = $this->Feed_Settings_Model->getStoreIdByStoreName($_SESSION['shop']); ?>
        <?php echo form_hidden('store_id', $getStoreCollection[0]->id); ?>
        <?php
        if ($records != NULL)
            echo form_hidden('id', $records[0]->id);
        ?>
		<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">              
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button>
						<img src="" class="imagepreview" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
        <fieldset>
			<div class="form-group" >
                <?php echo form_label('Url'); ?><div class="controls">
                    <?php echo form_input(array('id' => 'single_tweet_url', 'name' => 'single_tweet_url', 'class' => 'form-control', 'value' => ($records == NULL ? '' : $records[0]->href_url), 'placeholder' => 'Enter Tweet Url ')); ?>
                </div>
				<span id="single_tweet_url_error" style="color:red; display:none;">Required Field</span>				<i class="comment">Place your twitter specific URL like <b>https://twitter.com/[Your twitter handle]/status/123456789</b> and save the settings. Please refer to the preview.</i>
            </div>
          <?php if ($records != null) { ?>
				<div class="form-group shotcode">
                    <?php echo form_label('Shortcode: <span>(Paste this shortcode in theme files or any page)</span> See Example: <a class="shortcode1" image-src="http://zestardshop.com/shopifyapp/twitterfeed/zestard/assets/images/tweetpage.png" href="javascript:void(0)">Page</a> and <a class="shortcode1" href="javascript:void(0)" image-src="http://zestardshop.com/shopifyapp/twitterfeed/zestard/assets/images/themepage.png">Theme Files</a> '); ?>
                    <div class="showCodeWrapper">
                        <btn class="btn btn-default copyMe" onclick="copyToClipboard()" style="display: block;"><i class="fa fa-check"></i> Copied</btn>
                        <textarea id="shortcode3" rows="4" class="form-control short-code" data-app-type="banner-slider" readonly=""><div class="zestard-single-embedded-tweet" id="<?php echo $records[0]->crypt_id; ?>"></div>
                        </textarea></div>
                </div>
            <?php } ?>  
				
			<div class="form-group">
			<div class="form-group">
				<div class="panel panel-danger">
					<div class="panel-heading"><b>Note</b></div>
					<div class="panel-body">
						<ul>
							<li>In the URL field place your any single tweet link <b>Example: <a href="javascript:void(0);">https://twitter.com/zestardtech/status/696967267372953600</a> </b></li>
							<li>After Save the configuration you will see the <b> Preview </b> on left side.</li>
							<li>After saving the details, you will getting the <b> Shortcode </b> just copied it and you can paste anywhere on your site.    <a class="shortcode1" href="javascript:void(0);" image-src="<?php echo base_url('/assets/images/single.png') ?>"><b>See Example</b></a>
							</li>
							<li>If you want to show your <b>Twitter Timeline</b> in any page click on <a href="<?php echo 'https://'.$_SESSION['shop'].'/admin/pages/new'?>">Page</a> and Paste above shortcode in that page.   <a class="shortcode1" href="javascript:void(0)" image-src="<?php echo base_url('/assets/images/tweetpage.png') ?>"><b>See Example</b></a></li>
							<li>If you want to show your <b>Twitter Timeline</b> in specific place like in footer, just click on <a href="<?php echo 'https://'.$_SESSION['shop'].'/admin/themes/current/?key=layout/theme.liquid'?>">Theme.liquid</a> and Paste above shortcode in that file.<a class="shortcode1" href="javascript:void(0)" image-src="<?php echo base_url('/assets/images/themepage.png') ?>"><b>See Example</b></a> </li>
						</ul>
					</div>
				</div>
			</div>

               <div style="margin-top: 20px;">
                <?php echo form_submit(array('id' => 'submit', 'name' => 'submit', 'value' => 'Save Changes', 'class' => 'btn btn-primary custombutton')); ?>
            </div>
		</fieldset>
		<?php echo form_close(); ?>
    </div>

    <div class="col-md-4 col-xs-12">
        <h1 class="page-title">Preview Single Embedded Tweet</h1>
        <div id="custom_view_preview" class="custom-form-design"></div>		
    </div>
    <div class="col-md-4 col-xs-12" style="float:right;">
        <h1 style="font-size: 20px;
		color:#555;
		padding: 6px;
		margin: 10px 0px;
		text-align:center;
		width: 100%;
		border: 1px solid #e7e7e7;
		background: #e7e7e7;
		border-radius: 8px;">
		Field Description
		</h1> 
        <div class="custom-form-design">
            <ul>
                <li><strong>Url :</strong>Copy paste the specific URL you want to display in your widget window.</li>
            </ul>
        </div>
    </div>
</div>
<style>
    .custombutton{
    color: #fff !important;
    background-color: #555 !important;
    border-color: #e7e7e7 !important;
}
</style>
<script type="text/javascript">
    function copyToClipboard() {
        var st = document.getElementById("shortcode3").value;
        window.prompt("Copy to clipboard: Ctrl+C, Enter", st);
    }
	
	jQuery(function() {
		jQuery('.shortcode1').on('click', function() {
			jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
			jQuery('#imagemodal').modal('show');   
		});			
	});

    $('#submit').click(function(){
     var count = 0;
     var single_tweet_url = $('#single_tweet_url').val();
     
    if(single_tweet_url == ''){
        count++;
        $("span#single_tweet_url_error").show();
     }
     
    if(count == 0){
         return true;
     }else{
         return false;
     }
});

$(window).load(function() {

 if($('#shortcode3').val() != null && $('#shortcode3').val() != ''){
 var tweetview = $('#shortcode3').val();
 $('div#custom_view_preview').html(tweetview);

    var id = jQuery('.zestard-single-embedded-tweet').attr('id');
    jQuery.ajax({

        url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check_single_tweet/single_embedded_tweet_preview?id=' + id,
        dataType: "json",
        method: "get",
        success: function (res) {
                var id;
                id = jQuery(".zestard-single-embedded-tweet").attr('id');
                if(id == res.crypt_id ){
                    jQuery(".zestard-single-embedded-tweet").html(res[0]);
                }else{
                    jQuery(".zestard-single-embedded-tweet").html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store name did not match with current store '+res.store_name+'</div>');
                }
        },
        error: function (xhr, status, err) {
            jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                    .appendTo(document.body);
        }
    });
}else{
    /*	var id = jQuery('.zestard-single-embedded-tweet').attr('id');
    jQuery.ajax({
        url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check_single_tweet/single_embedded_tweet_preview?id=' + id,
        dataType: "json",
        method: "get",
        success: function (res) {
                $('div#custom_view_preview').html(res[0]);
        },
        error: function (xhr, status, err) {
            jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                    .appendTo(document.body);
        }
    });	*/	$('#custom_view_preview').html('<img src="<?php echo base_url('/assets/images/single-twitter.png') ?>" class="demo-img">');
}
});
</script>
