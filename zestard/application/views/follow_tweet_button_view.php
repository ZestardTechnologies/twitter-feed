
<div id="wrap">
    <div class="col-md-8 col-xs-12">
        <h3 class="page-title">Manage Share Button</h3>

        <?php echo form_open('Settings/Follow_button_tweet_add', array('name' => 'single_tweet', 'class' => 'custom-form-design', 'style' => 'border: 1px solid #ccc;padding: 20px;')); ?>
        <?php $getStoreCollection = $this->Feed_Settings_Model->getStoreIdByStoreName($_SESSION['shop']); ?>
        <?php echo form_hidden('store_id', $getStoreCollection[0]->id); ?>
        <?php
        if ($records != NULL)
            echo form_hidden('id', $records[0]->id);
        ?>
        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">              
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button>
						<img src="" class="imagepreview" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
        <fieldset>
            <div class="form-group" >
                <?php echo form_label('Button size'); ?>
                <div class="controls">
                    <select id="datasize" name="tweet_datasize[]" class ="form-control" >
                         <option value="">Select Size..</option>
                         <option value="1" <?php if(isset ($records[0]->tweet_datasize) && $records[0]->tweet_datasize == 1){echo 'selected';}else{ echo 'large'; } ?>>Large</option>
                         <option value="2" <?php if(isset ($records[0]->tweet_datasize) && $records[0]->tweet_datasize == 2){echo 'selected';}else{ echo 'small'; } ?>>Small</option>
                        </select>
                    <span id="datasize_error" style="color:red; display:none;">Required Field</span>

                </div>
            </div>
            <div class="form-group" >
                <?php echo form_label('Predefined Share Text'); ?>

                <div class="controls">
                    <?php echo form_input(array('id' => 'tweet_data_text', 'name' => 'tweet_data_text', 'class' => 'form-control', 'value' => ($records == NULL ? 'custom share text' : $records[0]->tweet_data_text), 'placeholder' => 'Enter Tweet button data text')); ?>
                </div>
                  <span id="tweet_data_text_error" style="color:red; display:none;">Required Field</span>
            </div>
           
             <div class="form-group" >
                <?php echo form_label('Predefined hashtag'); ?>
                  <div class="controls">
                    <?php echo form_input(array('id' => 'tweet_data_hastag', 'name' => 'tweet_data_hastag', 'class' => 'form-control', 'value' => ($records == NULL ? 'example,demo' : $records[0]->data_hashtags), 'placeholder' => 'Enter Tweet data-hashtags')); ?>
                </div>
                  <span id="tweet_data_hastag_error" style="color:red; display:none;">Required Field</span>
            </div>
             <div class="form-group" >
                <?php echo form_label('Share Via'); ?>
                  <div class="controls">
                    <?php echo form_input(array('id' => 'tweet_data_via', 'name' => 'tweet_data_via', 'class' => 'form-control', 'value' => ($records == NULL ? 'twitterdev' : $records[0]->tweet_data_via), 'placeholder' => 'Enter Tweet data-Via')); ?>
                </div>
                  <span id="tweet_data_hastag_error" style="color:red; display:none;">Required Field</span>
            </div>
             
        <?php if ($records != null) { ?>

                <div class="form-group shotcode">				
                    <?php echo form_label('Shortcode: <span>(Paste this shortcode in theme files or any page)</span> See Example: <a class="shortcode1" image-src="'.base_url('/assets/images/tweetpage.png').'" href="javascript:void(0)">Page</a> and <a class="shortcode1" href="javascript:void(0)" image-src="'.base_url('/assets/images/themepage.png').'">Theme Files</a> '); ?>
                    <div class="showCodeWrapper">
                        <btn class="btn btn-default copyMe" onclick="copyToClipboard()" style="display: block;"><i class="fa fa-check"></i> Copied</btn>
                        <textarea id="shortcode2" rows="4" class="form-control short-code" data-app-type="banner-slider" readonly=""><div class="zestard-single-tweetfeed" id="<?php echo $records[0]->crypt_id; ?>"></div></textarea>
    					</div>
                      </div>
            <?php } ?>
			<div class="form-group">
				<div class="panel panel-danger">
					<div class="panel-heading"><b>Note</b></div>
					<div class="panel-body">
						<ul>
							<li>Before you start filling a form just see this image, click on <a class="shortcode1" href="javascript:void(0);" image-src="<?php echo base_url('/assets/images/Share a link on Twitter.png') ?>"><b>See Example</b></a>
							then fill the above form and click on <b> Save Changes </b> button to save your entered details.
							</li>
							<li>After Saving the configurations you will be able to see the <b> Preview </b> on the left side.</li>
							<li>Once details are saved, you will get <b> Shortcode </b> which you can copy and can paste it anywhere on your site.<a class="shortcode1" href="javascript:void(0);" image-src="<?php echo base_url('/assets/images/button.png') ?>"><b>See Example</b></a>
							</li>
							<li><b>Twitter Timeline</b> in any page click on <a href="<?php echo 'https://'.$_SESSION['shop'].'/admin/pages/new'?>">Page</a> and Paste above shortcode in that page.   <a class="shortcode1" href="javascript:void(0)" image-src="<?php echo base_url('/assets/images/tweetpage.png') ?>"><b>See Example</b></a></li>
							<li>If you want to show your <b>Twitter Timeline</b> in specific place like in footer, just click on <a href="<?php echo 'https://'.$_SESSION['shop'].'/admin/themes/current/?key=layout/theme.liquid'?>">Theme.liquid</a> and Paste above shortcode in that file.   <a class="shortcode1" href="javascript:void(0)" image-src="<?php echo base_url('/assets/images/themepage.png') ?>"><b>See Example</b></a> </li>
						</ul>
					</div> 
				</div>	
			</div>

            <div style="margin-top: 20px;">
                <?php echo form_submit(array('id' => 'submit', 'name' => 'submit', 'value' => 'Save Changes', 'class' => 'btn btn-primary custombutton')); ?>
            </div>

        </fieldset>
        <?php

        ?>
<?php echo form_close(); ?>
    </div>

    <div class="col-md-4 col-xs-12">
        <h3 class="page-title">Preview Share Button</h3>
        <div id="custom_view_preview" class="custom-form-design">

        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <h3 class="page-title">Field Description</h3>
        <div class="custom-form-design">
            <ul>
                <li><strong>Button Size :</strong> Define Size type Large/Medium/Small of Follow Button.</li>
                <li><strong>Predefined Share Text :</strong> Choose size of text on Follow Button.</li>
                <li><strong>Predefined hashtag :</strong> Choose the type of hashtag for follow button.</li>
                <li><strong>Share via :</strong> Choose the channel through which you want to route the follow requests.</li>
            </ul>
            <p>For reference see the image.Click <a class="test-popup-link" href="<?php echo base_url('/assets/images/Share a link on Twitter.png') ?>">Here</a></p>
        </div>
    </div>
</div>
<style>
    .custombutton{
    color: #fff !important;
    background-color: #555 !important;
    border-color: #e7e7e7 !important;
}
</style>
<script type="text/javascript">
    function copyToClipboard() {
        var st = document.getElementById("shortcode2").value;
        window.prompt("Copy to clipboard: Ctrl+C, Enter", st);
    }
	
	jQuery(function() {
		jQuery('.shortcode1').on('click', function() {
			jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
			jQuery('#imagemodal').modal('show');   
		});			
	});
	
    
    $('#submit').click(function(){
     var count = 0;
     var datasize = $('#datasize').val();
     var tweet_data_text = $('#tweet_data_text').val();
     var tweet_data_hastag = $('#tweet_data_hastag').val();
     

     if(datasize == ''){
        count++;
        $("span#datasize_error").show();
     }
    if(tweet_data_text == ''){
        count++;
        $("span#tweet_data_text_error").show();
     }
     if(tweet_data_hastag == ''){
        count++;
        $("span#tweet_data_hastag_error").show();
    }
    if(count == 0){
         return true;
     }else{
         return false;
     }
});

$(window).load(function() {
     
  if($('#shortcode2').val() != null && $('#shortcode2').val() != ''){
        var tweetview = $('#shortcode2').val();
 $('div#custom_view_preview').html(tweetview);

    var id = jQuery('.zestard-single-tweetfeed').attr('id');
    jQuery.ajax({
        url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check_single_tweet/tweet_preview?id=' + id,
        dataType: "json",
        method: "get",
        success: function (res) {
                var id;
                id = jQuery(".zestard-single-tweetfeed").attr('id');
                if(id == res.crypt_id ){
                    jQuery(".zestard-single-tweetfeed").html(res[0]);
                }else{
                    jQuery(".zestard-single-tweetfeed").html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store name did not match with current store '+res.store_name+'</div>');
                }
        },
        error: function (xhr, status, err) {
            jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                    .appendTo(document.body);
        }
    });
}else{
    var id = jQuery('.zestard-single-tweetfeed').attr('id');
    jQuery.ajax({
        url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check_single_tweet/tweet_preview?id=' + id,
        dataType: "json",
        method: "get",
        success: function (res) {
                $('div#custom_view_preview').html(res[0]);
        },
        error: function (xhr, status, err) {
            jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                    .appendTo(document.body);
        }
    });
}
    $('.test-popup-link').magnificPopup({
      type: 'image'
      // other options
    });
});
</script>
