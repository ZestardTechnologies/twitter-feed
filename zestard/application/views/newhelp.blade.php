@extends('header')
@section('content')
<?php $store_name = session('shop'); ?>
<link rel="stylesheet" href="<?php echo base_url($css . 'design_style.css'); ?>">
<div class="container formcolor formcolor_help" >
    <div class=" row">
        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">              
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <img src="" class="imagepreview" style="width: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <div class="help_page">            
            <div class="col-md-12 col-sm-12 col-xs-12 need_help">
                <div class="success-copied"></div>
                <h2 class="dd-help">Need Help?</h2>
                <p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="dd-help-ul">
                        <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                        <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                        <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">Configuration Instruction</h2> 
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                                        <strong><span class="">How to configure General Setting ?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>You can easily Enable / Disable the app in <b>"Enable App?"</b> field.</li>
                                        <li>Select the type of slider as you like in <b>"Select Slider Type"</b> feild.</li>
                                        <li>Write the Blog page title in <b>"Blog Page Title"</b> field.</li>
                                        <li>Write the Product page title in <b>"Product Page Title"</b> field.</li>
                                    </ul>                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                                        <strong><span class="">How to show Blogs on Product page ?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>First write the number of blogs per row you want to show.</li>
                                        <li>If you have not assign blogs to products in <b>"PRODUCTS"</b> on top of the app, then check the check-box of display random blogs in product page.</li>
                                        <li>Then copy the short-code from below and paste into <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a>. <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/product-template.liquid.png') }}"><b> See Example</b></a>.</li>

                                    </ul>
                                    <div class="copystyle_wrapper col-md-5">
                                        <textarea rows="1" class="form-control popup_code" id="product-shortcode" disabled><?php echo "<div class='zestard-bloglist' store_id='" . $usersettings[0]->store_encrypt . "' product_id='{{ product.id }}'></div>" ?></textarea>
                                        <btn id="copyproductBtn" name="copyproductBtn" value="Copy Popupcode" class="btn btn-info copycss_button copyMe" data-clipboard-target="#product-shortcode" style="display: block;" onclick="copyToClipboard('#product-shortcode')"><i class="fa fa-check"></i> Copy</btn>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                                        <strong><span class="">How to show Products on Blog page ?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>First write the number of products per row you want to show.</li>
                                        <li>If you have not assign products to blog in <b>"BlOGS"</b> on top of the app, then check the check-box of display random products in blog page.</li>
                                        <li>Then copy the short-code from below and paste into <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/article-template.liquid" target="_blank"><b>article-template.liquid</b></a>. <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/article-template.liquid.png') }}"><b> See Example</b></a>.</li>
                                    </ul>
                                    <div class="copystyle_wrapper col-md-5">
                                        <textarea rows="1" class="form-control popup_code" id="blog-shortcode" disabled><?php echo "<div class='zestard-productlist' store_id='" . $usersettings[0]->store_encrypt . "' blog_id='{{ article.id }}'></div>" ?></textarea>
                                        <btn id="copyblogBtn" name="copyblogBtn" value="Copy Popupcode" class="btn btn-info copycss_button copyMe" data-clipboard-target="#blog-shortcode" style="display: block;" onclick="copyToClipboard('#blog-shortcode')"><i class="fa fa-check"></i> Copy</btn>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                                        <strong><span class="">How to assgin Blogs to particular product?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>Go to <b>"PRODUCTS"</b> on top of the app.</li>
                                        <li>Search for product in which you want to assign blogs and click on edit.</li>
                                        <li>Now select the blogs which you want to assign on particular product and click on submit.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse5"> 
                                        <strong><span class="">How to assign Products to particular blog?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>Go to <b>"BLOGS"</b> on top of the app.</li>
                                        <li>Search for blog in which you want to assign products and click on edit.</li>
                                        <li>Now select the Products which you want to assign on particular blog and click on submit.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <!--Uninstall Process Div start-->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">Uninstall Instruction</h2> 
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">                        
                        <ul class="ul-help">
                            <li>To uninstall the app, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                            <li>Click on delete icon of Related Products and Blogs.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/uninstallApp.png') }}"><b> See Example</b></a></li>
                            <li>If possible remove the shortcode from <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a> and <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/article-template.liquid" target="_blank"><b>article-template.liquid</b></a> files.</li>
                        </ul>                        
                    </div>
                </div>
            </div>
            <!--Uninstall Process Div end-->
        </div>
        <!--Version updates-->
        <!--        <div class="version_update_section">
                    <div class="col-md-6" style="padding-right: 0;">
                        <div class="feature_box">
                            <h3 class="dd-help">Version Updates <span class="verison_no">2.0</span></h3>
                            <div class="version_block">
                                <div class="col-md-12">
                                    <div class="col-md-3 version_date">
                                        <p><i class="glyphicon glyphicon-heart"></i></p>
                                        <strong>22 Jan, 2018</strong>
                                        <a href="#"><b>Update</b></a>
                                    </div>
                                    <div class="col-md-8 version_details">
                                        <strong>Version 2.0</strong>
                                        <ul>
                                            <li>Add Delivery Date & Time information under customer order Email</li>
                                            <li>Auto Select for Next Available Delivery Date</li>
                                            <li>Auto Tag Delivery Details to all the Orders</li>
                                            <li>Manage Cut Off Time for Each Individual Weekday</li>
                                            <li>Limit Number of Order Delivery during the given time slot for any day </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3 version_date">
                                        <p><i class="glyphicon glyphicon-globe"></i></p>
                                        <strong>20 Dec, 2017</strong>
                                        <a href="#"><b>Release</b></a>
                                    </div>
                                    <div class="col-md-8 version_details version_details_2">
                                        <strong>Version 1.0</strong>
                                        <ul>
                                            <li>Delivery Date & Time Selection</li>
                                            <li>Same Day & Next Day Delivery</li>
                                            <li>Blocking Specific Days and Dates</li>
                                            <li>Admin Order Manage & Export Based on Delivery Date</li>
                                            <li>Option for Cut Off Time & Delivery Time</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="feature_box">
                            <h3 class="dd-help">Upcoming Features</h3>
                            <div class="feature_block">
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox0" name="exclude_block_date_status">
                                        <label for="checkbox0"></label>
                                    </span> 
                                    <strong>Multiple Cutoff Time Option</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox4" name="exclude_block_date_status">
                                        <label for="checkbox4"></label>
                                    </span> 
                                    <strong>Multiple Delivery Time Option</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox5" name="exclude_block_date_status">
                                        <label for="checkbox5"></label>
                                    </span> 
                                    <strong>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox1" name="exclude_block_date_status">
                                        <label for="checkbox1"></label>
                                    </span> 
                                    <strong>Auto Select for Next Available Delivery Date</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox2" name="exclude_block_date_status">
                                        <label for="checkbox2"></label>
                                    </span> 
                                    <strong>Order Export in Excel</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox3" name="exclude_block_date_status">
                                        <label for="checkbox3"></label>
                                    </span> 
                                    <strong>Filtering Orders by Delivery Date</strong>  
                                </div>                                        
                            </div>
                            <div>
                                <p class="feature_text">
                                    New features are always welcome send us on : <a href="mailto:support@zestard.com"><b>support@zestard.com</b></a> 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>-->

    </div>
</div>
<script>
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");
        });
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    });
</script>
<script>
    var el1 = document.getElementById('copyproductBtn');
    var el2 = document.getElementById('copyblogBtn');
    var el3 = document.getElementById('copyrandomproductBtn');
    if (el1) {
        el1.addEventListener("click", function () {
            copyToClipboard(document.getElementById("product-shortcode"));
        });
    }
    if (el2) {
        el2.addEventListener("click", function () {
            copyToClipboard(document.getElementById("blog-shortcode"));
        });
    }
    if (el3) {
        el3.addEventListener("click", function () {
            copyToClipboard(document.getElementById("randomproduct-shortcode"));
        });
    }
    /*document.getElementById("copyproductBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("product-shortcode"));
     });
     document.getElementById("copyblogBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("blog-shortcode"));
     });
     document.getElementById("copyrandomproductBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("randomproduct-shortcode"));
     });*/


    function copyToClipboard(elem) {
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }
        if (isInput) {
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            target.textContent = "";
        }
        return succeed;
    }
</script>
@endsection
