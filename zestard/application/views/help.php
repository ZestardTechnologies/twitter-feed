<div id="wrap">	
	<div class="col-md-8 col-xs-12"> 
		<h1 class="info-head">How to Use ?</h1>
		<div class="alert alert-info custom-fonts" style="background: #d3dbe2; color: #494949; border: 1px solid #ccc">						
			<div>
				1) First go to <a href="<?php echo base_url('/Settings'); ?>">"Time-line Settings"</a> section and click on that.
					<ul>
						<li>Then You have to show the default view with the preview.</li>
						<li>Its just notifying user which kind of data you have to provide in the input box.</i>
						<li>You can Fill up the input data by your self for your twitter timeline widget display. </i>
                                                <li>Then click on the save button.</li>
                                                <li>After that You can show the preview of the fillable data. </i>
						
					</ul>
			</div>
			<div>
				2) In that Apps Also You can set the tweet Button, Go to <a href="<?php echo base_url('/Settings/follow_button_tweet'); ?>">"Follow Button Setting Tab"</a> section click on that<br/>
				   <ul>
						<li>Then set all the data according to your information</li>  
						<li>Data size must be required.</i>
						<li>Default size is small you can change it.</li>
						<li>Other input fields fill up according to set your tweet display</li>
                                                <li>Then click on the save button.</li>
                                                <li>After that You can show the preview of the fillable data. </i>
					</ul>
			</div>
                        
			<div>
				3) You can also set the single embedded tweet, Now go to the <a href="<?php echo base_url('/Settings/Single_embedded_tweet'); ?>">"Single Embedded Setting"</a> section click on that.<br/>
				   	<ul>
                                                <li>You can see the default single embedded preview.</li>
                                            	<li>Then set the data-url if whatever tweet you can set the in the list of tweet that will be display in front view.</li>
						<li>Data data-Url must be required.</i>
						<li>Then click on the save button.</li>
                                                <li>After that You can see the preview of the Data Url. </i>
					</ul>
			</div>
                        <div>
                            4) Above Selected Tab when You click on save button generate new Shortcode.
                            <ul>
                                <li>You can see the label like "Paste this shortcode in any page or post"</li>
                                <li>Just click on the "Copied" button then copy the whole short HTML code.</i>
                                <li>After copying, you can paste it in any page according to your requirement.</li>
                            </ul>
                        </div>
			<div>
				<ul>
					<li>Now You can see the according to short-code different different tweet in your front view.</li>
				</ul>				
			</div>			
		</div>		
	</div>
	<div class="col-md-4 col-xs-12">
		<div>
			<h1 class="info-head">Development Center</h1>
			<a href="http://www.zestard.com/" target="_blank" class="development-center">
				<img src="<?php echo base_url('/assets/images/zestard-logo.png') ?>" alt="Zestard"/>
			</a>
			<div class="alert alert-info" style="background: #d3dbe2; color: #494949; border: 1px solid #ccc">			
				<p><strong style="float:left">Email: </strong><a href="mailto:shopify@zestard.com">&nbsp;support@zestard.com</a></p>			
			</div>
		</div>
		<div>
			<h1 class="info-head">Benefits</h1>
			<div class="alert alert-info" style="background: #d3dbe2; color: #494949; border: 1px solid #ccc">		
				<ul class="benefits">
					<li>Easy To Use</li>
					<li>You can use timeline tweet widget set your own height width additionally as set color.</li>
					<li>You can set the Tweet button in line with providing your size.</li>
					<li>Responsive (Mobile/tablet Friendly) Single also as multiple tweet show.</li>
					<li>You can set the tweet feed set in the footer.</li>
					<li>Tweet feed automatically generates short code and Admin will paste it where it needs to be shown in any page.</li>
				</ul>				
			</div>
                        
		</div>
	</div>
</div>
<script type="text/javascript">
	function copyToClipboard() {
        var st = document.getElementById("shortcode").value;
        window.prompt("Copy to clipboard: Ctrl+C, Enter", st);}
</script>




