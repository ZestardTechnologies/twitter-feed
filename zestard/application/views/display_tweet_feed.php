<style>
.twitter-timeline{
	min-width:auto !important;
	width:100% !important;
}
.tw-feed {
	height:<?php if(isset ($result[0]->tweet_box_height) && $result[0]->tweet_box_height != ''){echo $result[0]->tweet_box_height;}else{echo '350';} ?>px;
	width:<?php  if(isset ($result[0]->tweet_box_width) && $result[0]->tweet_box_width != '') {echo $result[0]->tweet_box_width;}else{echo '100';} ?>%;
	color:#ff0000;
	padding:10px;
	overflow-y: scroll;
	margin:auto;
	border: solid 2px <?php if(isset ($result[0]->border_color) && $result[0]->border_color != '') {echo $result[0]->border_color; } else{echo '#ffffff';}?>;
}
.tw-feed a {
	color:<?php if(isset ($result[0]->data_link_color) && $result[0]->data_link_color != ''){echo $result[0]->data_link_color; }else{ echo '#1f497d'; } ?>;
	 text-decoration:none;
}
.tw-feed a:hover {color:<?php if(isset ($result[0]->data_link_color) && $result[0]->data_link_color != ''){echo $result[0]->data_link_color;}else{echo '#1f497d';} ?>
	   text-decoration:none;
}
</style>
<div class="tw-feed">
	<a style="max-width: auto;" class="twitter-timeline" href="https://twitter.com/<?php if(isset ($result[0]->feed_name) && $result[0]->feed_name != ''){echo $result[0]->feed_name;} else{ echo 'zestardtech'; }?>"
	data-theme="<?php if(isset ($result[0]->feed_theme) && $result[0]->feed_theme != ''){echo $result[0]->feed_theme;}else{echo '100';} ?>"
	data-link-color="<?php if(isset ($result[0]->data_link_color) && $result[0]->data_link_color != ''){echo $result[0]->data_link_color;} else{echo '350';}?>"
	background="<?php if(isset ($result[0]->feed_background) && $result[0]->feed_background != '') {echo $result[0]->feed_background;}else{echo 'light';} ?>"
	border-radius="none"
	data-chrome="nofooter noborders"
	data-widget-id="413723523169787904"
	data-screen-name="<?php if(isset ($result[0]->feed_name) && $result[0]->feed_name != ''){echo $result[0]->feed_name;}else{echo 'zestardtech';}  ?>" data-show-replies="true"
	data-tweet-limit="<?php if(isset ($result[0]->feed_tweet_limit) && $result[0]->feed_tweet_limit != ''){ echo $result[0]->feed_tweet_limit; }else{ echo '12';}  ?>"
	@<?php if(isset ($result[0]->feed_name) && $result[0]->feed_name != ''){ echo $result[0]->feed_name; } else{echo 'zestardtech';} ?>> </a></div>
<script type="text/javascript">!function(d,s,id){
	var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){		js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";
		fjs.parentNode.insertBefore(js,fjs);
	}
}(document,"script","twitter-wjs");</script>

   