<?php
//session_start();
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <!--        Load the CSS                 -->
        <link rel="stylesheet" href="<?php echo base_url($css . 'font-awesome.min.css'); ?>">
        <link id="jquiCSS" rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/ui-lightness/jquery-ui.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?php echo base_url($css . 'evol-colorpicker.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($css . 'style.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($css . 'bootstrap.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url($css . 'magnific-popup.css'); ?>">
        
        <!--        Load the JS                 -->
        <script type="text/javascript" src="<?php echo base_url($js . 'jquery.min.js'); ?>"></script>
         <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
        <script type="text/javascript" src="<?php echo base_url($js . 'bootstrap.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url($js . 'jquery.validate.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url($js . 'evol-colorpicker.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url($js . 'jquery.magnific-popup.js'); ?>"></script>
       
        <?php
            $api_key = $_SESSION['api_key']; //$this->session->userdata('api_key');
            $shop = $_SESSION['shop']; //$this->session->userdata('shop');
        ?>
        <script type="text/javascript">			
			ShopifyApp.init({
                apiKey: '<?php if(isset ($api_key)){echo $api_key;} ?>',
				shopOrigin: 'https://<?php if(isset ($shop)){ echo $shop; }?>'
			});	
			ShopifyApp.ready(function() {
                        ShopifyApp.Bar.initialize({
		        icon: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/assets/images/icon.png',
		        title: 'Management',
		        buttons: { }
		      });
		    });		    
	</script> 
        
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->
    <title><?php echo $title; ?></title>
    
    </head>    
