<div id="wrap">		
    <div class="col-md-8 col-xs-12">
        <h3 class="page-title">Manage Tweet Feed Settings </h3>

        <?php echo form_open('Settings/add', array('name' => 'registration', 'class' => 'custom-form-design', 'style' => 'border: 1px solid #ccc;padding: 20px;')); ?>
        <?php $getStoreCollection = $this->Feed_Settings_Model->getStoreIdByStoreName($_SESSION['shop']); ?>		    	
        <?php echo form_hidden('store_id', $getStoreCollection[0]->id); ?>	
        <?php
        if ($records != NULL)
            echo form_hidden('id', $records[0]->id);
        ?>
		<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">              
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button>
						<img src="" class="imagepreview" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
		
        <fieldset>

            <div class="form-group">
                <?php echo form_label('Feed Name'); ?>					
                <div class="controls">
                    <?php echo form_input(array('id' => 'feed_name', 'name' => 'feed_name', 'class' => 'form-control', 'value' => ($records == NULL ? '' : $records[0]->feed_name), 'placeholder' => 'Enter Feed Name')); ?>
                </div>
                <span id="feed_name_error" style="color:red; display:none;">Required Field</span>
				<i class="comment">Place your twitter handle suppose for example <b>zestardtech</b> and save the settings. Refer to preview section to see the changes.</i>
            </div>

            <div class="form-group" >
                <?php echo form_label('Feed Box Width(%)'); ?>					

                <div class="controls">
                    <?php echo form_input(array('id' => 'tweet_box_width', 'name' => 'tweet_box_width', 'class' => 'form-control', 'value' => ($records == NULL ? '100' : $records[0]->tweet_box_width), 'placeholder' => 'Enter Box Width')); ?>
                </div>
                <span id="tweet_box_width_error" style="color:red;display:none;">Required Field</span>
            </div>
            <div class="form-group" >
                <?php echo form_label('Feed Box Height'); ?>

                <div class="controls">
                    <?php echo form_input(array('id' => 'tweet_box_height', 'name' => 'tweet_box_height', 'class' => 'form-control', 'value' => ($records == NULL ? '350' : $records[0]->tweet_box_height), 'placeholder' => 'Enter Box Height')); ?>
                </div>
                <span id="tweet_box_height_error" style="color:red;display:none;">Required Field</span>
            </div>

            <div class="form-group">
                <?php echo form_label('Feed Theme (light, dark)'); ?>					
                <div class="controls">
                    <?php echo form_input(array('id' => 'feed_theme', 'name' => 'feed_theme', 'class' => 'form-control', 'value' => ($records == NULL ? 'light' : $records[0]->feed_theme), 'placeholder' => 'Enter Feed Theme')); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo form_label('Feed Limit'); ?>					
                <div class="controls">
                    <?php echo form_input(array('id' => 'feed_tweet_limit', 'name' => 'feed_tweet_limit', 'value' => ($records == NULL ? '12' : $records[0]->feed_tweet_limit), 'class' => 'form-control', 'placeholder' => 'Enter Feed Limit')); ?>
                </div>
                <span id="feed_theme_error" style="color:red;display:none;">Required Field</span>
            </div>

            <div class="form-group">
                <?php echo form_label('Data Link Color'); ?>					
                <div class="controls">
                    <?php echo form_input(array('id' => 'data_link_color', 'class' => 'form-control form-color-picker noIndColor', 'name' => 'data_link_color', 'value' => ($records == NULL ? '#1f497d' : $records[0]->data_link_color))); ?>
                </div>
                <span id="data_link_color_error" style="color:red;display:none;">Required Field</span>


            </div>


            <div class="form-group">
                <?php echo form_label('Feed Background'); ?>	
                <div class="controls">
                    <?php echo form_input(array('id' => 'feed_background', 'name' => 'feed_background', 'value' => ($records == NULL ? '' : $records[0]->feed_background), 'class' => 'form-control form-color-picker noIndColor')); ?>
                </div>
            </div>


            <div class="form-group">
                <?php echo form_label('Border Color'); ?>					
                <div class="controls">
                    <?php echo form_input(array('id' => 'border_color', 'name' => 'border_color', 'value' => ($records == NULL ? '#548dd4' : $records[0]->border_color), 'class' => 'form-control form-color-picker noIndColor')); ?>
                </div>
            </div>

            <?php if ($records != null) { ?>
                <div class="form-group shotcode">
                    <?php echo form_label('Shortcode: <span>(Paste this shortcode in theme files or any page)</span> See Example: <a class="shortcode1" image-src="http://zestardshop.com/shopifyapp/twitterfeed/zestard/assets/images/tweetpage.png" href="javascript:void(0)">Page</a> and <a class="shortcode1" href="javascript:void(0)" image-src="http://zestardshop.com/shopifyapp/twitterfeed/zestard/assets/images/themepage.png">Theme Files</a> '); ?>
					<!--<a href="https://'.$_SESSION['shop'].'/admin/themes/current/?key=layout/theme.liquid" target="_blank">Theme.liquid</a> -->
					<br/>
                    <div class="showCodeWrapper">
                        <btn class="btn btn-default copyMe" onclick="copyToClipboard()" style="display: block;"><i class="fa fa-check"></i> Copied</btn>
                        <textarea id="shortcode" rows="4" class="form-control short-code" data-app-type="banner-slider" readonly=""><div class="zestard-tweetfeed" id="<?php echo $records[0]->crypt_id; ?>"></div></textarea>
					</div>
				</div>
            <?php } ?>
						
			<div class="form-group">
				<div class="panel panel-danger">
					<div class="panel-heading"><b>Note</b></div>
					<div class="panel-body">
						<ul>
							<li>Please fill all the above form details and click <b> Save Changes </b> button to save all the details.</li>
							<li>Once all the details are saved, please have a look on the <b> Preview </b> section in the left side. </li>
							<li>After saving the details, you will get <b> Shortcode. </b> which can be copied and can be paste anywhere on your site.
							<a class="shortcode1" href="javascript:void(0)" image-src="<?php echo base_url('/assets/images/copy.png') ?>">Please<b>See Example</b></a>
							</li>
							<li>If you want to show your <b>Twitter Timeline</b> in any page click on <a href="<?php echo 'https://'.$_SESSION['shop'].'/admin/pages/new'?>">Page</a> and Paste above shortcode in that page.<a class="shortcode1" href="javascript:void(0)" image-src="<?php echo base_url('/assets/images/tweetpage.png') ?>"><b>See Example</b></a></li>
							<li>If you want to show your <b>Twitter Timeline</b> in specific place like in footer, just click on <a href="<?php echo 'https://'.$_SESSION['shop'].'/admin/themes/current/?key=layout/theme.liquid'?>">Theme.liquid</a> and Paste above shortcode in that file.   <a class="shortcode1" href="javascript:void(0)" image-src="<?php echo base_url('/assets/images/themepage.png') ?>"><b>See Example</b></a> </li>
						</ul>
					</div>
				</div>
			</div>
             
               <div style="margin-top: 20px;">
                <?php echo form_submit(array('id' => 'submit', 'name' => 'submit', 'value' => 'Save Changes', 'class' => 'btn btn-primary custombutton')); ?>
            </div>	
              
        </fieldset>
		
<?php echo form_close(); ?>
    </div>

    <div class="col-md-4 col-xs-12">
        <h3 class="page-title">Preview Tweet Feed Box</h3>
        <div id="custom_view_preview" class="custom-form-design"> 
            
        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <h3 class="page-title">Field Description</h3>
        <div class="custom-form-design">
            <ul>
                <li><strong>Feed Name :</strong> Name of the Feed you want to display in your widget window.</li>
                <li><strong>Feed Box Width :</strong> Choose the width of the window box.</li>
                <li><strong>Feed Box Height :</strong> Choose the height of the window box.</li>
                <li><strong>Feed Theme :</strong> Choose the theme light/dark for the widget box.</li>
                <li><strong>Feed Limit :</strong> Choose the number of feeds you would like to display in feed window.</li>
                <li><strong>Data Light Color :</strong> Choose the color of text data you would like to display in feed.</li>
                <li><strong>Feed Background :</strong> Choose Feed background color.</li>
                <li><strong>BorderColor :</strong> Choose color for border of the window.</li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
    function copyToClipboard() {
        var st = document.getElementById("shortcode").value;
        window.prompt("Copy to clipboard: Ctrl+C, Enter", st);
    }
	
	jQuery(function() {
		jQuery('.shortcode1').on('click', function() {
			jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
			jQuery('#imagemodal').modal('show');   
		});			
	});
	
    $(document).ready(function () {
        $('.noIndColor').colorpicker({
            displayIndicator: false
        });
    });
    $('#submit').click(function(){
        var count = 0;

     var feed_name = $('#feed_name').val();
     var tweet_box_width = $('#tweet_box_width').val();
     var tweet_box_height = $('#tweet_box_height').val();
     var feed_theme = $('#feed_theme').val();
     var feed_tweet_limit = $('#feed_tweet_limit').val();
     var data_link_color = $('#data_link_color').val();
     var feed_background = $('#feed_background').val();
     var border_color = $('#border_color').val();

     if(feed_name == ''){
        count++;
        $("span#feed_name_error").show();
     }
     if(tweet_box_width == ''){
        count++;
        $("span#tweet_box_width_error").show();
    }
    if(tweet_box_height == ''){
        count++;
        $("span#tweet_box_height_error").show();
    }
    if(data_link_color == ''){
        count++;
        $("span#data_link_color_error").show();
    }
    if(count == 0){
         return true;
     }else{
         return false;
     }
});

$(window).load(function() {
    $('.evo-cp-wrap').removeAttr('style');
    if($('#shortcode').val() != null && $('#shortcode').val() != ''){
        var tweetview = $('#shortcode').val();
        $('div#custom_view_preview').html(tweetview);
        var id = jQuery('.zestard-tweetfeed').attr('id');
        jQuery.ajax({
            url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check/admin_preview?id=' + id,
            dataType: "json",
            method: "get",
            success: function (res) {
                    var id;
                    id = jQuery(".zestard-tweetfeed").attr('id');
                    if(id == res.crypt_id ){
                        jQuery(".zestard-tweetfeed").html(res[0]);
                    }else{
                        jQuery(".zestard-tweetfeed").html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store'+ res.store_name +'did not match with current store </div>');
                    }
            },
            error: function (xhr, status, err) {
                jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                        .appendTo(document.body);
            }
        });
    } else {
		$('#custom_view_preview').html('<img src="<?php echo base_url('/assets/images/twitter-demo.png') ?>" class="demo-img">');
		/*
		var id = jQuery('.zestard-tweetfeed').attr('id');
		jQuery.ajax({
			url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check/admin_preview?id=' + id,
			dataType: "json",
			method: "get",
			success: function (res) {
					$('div#custom_view_preview').html(res[0]);
			},
			error: function (xhr, status, err) {
				jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
						.appendTo(document.body);
			}
		});
		*/
}

});
</script>
