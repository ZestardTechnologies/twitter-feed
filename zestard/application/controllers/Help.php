<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require __DIR__ . '/../../../vendor/shopifymodel/shopify_api/client.php';
//require __DIR__ . '/../../../vendor/shopifymodel/wcurl/wcurl.php';
use shopifymodel\shopify_api;
class Help extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index()
	{
            	$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Help Page'
                );
    	
    	$this->page = array( 
			'page'	=>	'help'
		);

		$this->load->view('themepart/head',$this->assets);
		$this->load->view('themepart/navigation',$this->page);
		$this->load->view('help');		
	}
        
}