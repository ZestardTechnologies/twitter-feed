<?php
//session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->model('Feed_Settings_Model');
        $this->load->model('Single_tweet_Model');
        
    }

    public function index() {
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Manage Settings'
        );

        $this->page = array('page' => 'settings');
        $data = $this->Feed_Settings_Model->getStoreIdByStoreNameFromController($_SESSION['shop']);
        $query = $this->db->get_where("tweet_feed_settings", array("store_id" => $data[0]->id));

        $data['records'] = $query->result();

        session_write_close();
        $this->load->view('themepart/head', $this->assets);
        $this->load->view('themepart/navigation', $this->page);
        $this->load->view('settings', $data);
    }

    public function add() {
        $data = array(
            'store_id' => $this->input->post('store_id'),
            'data_link_color' => $this->input->post('data_link_color'),
            'tweet_box_width' => $this->input->post('tweet_box_width'),
            'tweet_box_height' => $this->input->post('tweet_box_height'),
            'feed_name' => $this->input->post('feed_name'),
            'feed_background' => $this->input->post('feed_background'),
            'feed_tweet_limit' => $this->input->post('feed_tweet_limit'),
            'feed_theme' => $this->input->post('feed_theme'),
            'border_color' => $this->input->post('border_color')
        );

        $name = $this->input->post('id');

        if ($name == NULL)
            $this->Feed_Settings_Model->insert($data);
        else
            $this->Feed_Settings_Model->update($data, $name);

        redirect('/settings');
    }

    public function follow_button_tweet() {
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Manage settings-follow-button'
        );

        $this->page = array('page' => 'settings-follow-button');
        $data = $this->Feed_Settings_Model->getStoreIdByStoreNameFromController($_SESSION['shop']);
        $query = $this->db->get_where("single_tweet_setting", array("store_id" => $data[0]->id));

        $data['records'] = $query->result();
        session_write_close();
        $this->load->view('themepart/head', $this->assets);
        $this->load->view('themepart/navigation', $this->page);
        $this->load->view('follow_tweet_button_view', $data);
    }

    public function Follow_button_tweet_add() {
        $tweet_datasize = $this->input->post('tweet_datasize');
        foreach ($tweet_datasize as $value) {
            $data['tweet_datasize'] = $value;
        }
        $data['store_id'] = $this->input->post('store_id');
        $data['data_hashtags'] = $this->input->post('tweet_data_hastag');
        $data['tweet_data_text'] = $this->input->post('tweet_data_text');
        $data['tweet_data_via'] = $this->input->post('tweet_data_via');
        $name = $this->input->post('id');
        if ($name == NULL)
            $this->Single_tweet_Model->insert($data);
        else
            $this->Single_tweet_Model->update($data, $name);

        redirect('/settings/follow_button_tweet');
    }

    public function Single_embedded_tweet() {
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Manage-settings-sinlge-embedded-tweet'
        );

        $this->page = array('page' => 'settings-single-embedded-tweet');
        $data = $this->Feed_Settings_Model->getStoreIdByStoreNameFromController($_SESSION['shop']);
        $query = $this->db->get_where("single_embedded_tweet", array("store_id" => $data[0]->id));

        $data['records'] = $query->result();
        session_write_close();
        $this->load->view('themepart/head', $this->assets);
        $this->load->view('themepart/navigation', $this->page);
        $this->load->view('single_embedded_tweet_view', $data);
    }

    public function single_embedded_tweet_add() {
        $data['href_url'] = $this->input->post('single_tweet_url');
        $data['store_id'] = $this->input->post('store_id');

        $name = $this->input->post('id');
        if ($name == NULL) {
            $this->Single_tweet_Model->single_embedded_tweet_insert($data);
        } else {
            $this->Single_tweet_Model->single_embedded_tweet_update($data, $name);
        }

        redirect('/settings/Single_embedded_tweet');
    }

}