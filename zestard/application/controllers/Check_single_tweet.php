<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Check_single_tweet extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->database();

        $this->load->model('Tweet_Feed_Model');

    }



    public function index() {

        $this->assets = array(

            'css' => $this->config->item('css'),

            'js' => $this->config->item('js'),

            'title' => 'Single Tweet Feed'

        );

        $getId = explode(",", $_GET['ids']);

        $shop = $_GET['shop'];

        $getView = array();

            ob_start();

            $this->db->select('*');

            $this->db->from('usersettings');

            $this->db->join('single_tweet_setting','usersettings.id = single_tweet_setting.store_id');

            $this->db->where_in('single_tweet_setting.crypt_id',$getId);

            //$this->db->where('usersettings.store_name', $shop);
             $where = "usersettings.store_name ='".$shop."' OR usersettings.domain ='".$shop."'";
            $this->db->where($where);
            $data['result'] = $this->db->get();
            if($data['result']->num_rows() > 0){

                $data['result'] = $data['result']->result();

                $this->load->view('display_follow_tweet_view', $data);

                $output = ob_get_contents();

                ob_end_clean();

                $getView[] = $output;

                $getView['crypt_id'] = $data['result'][0]->crypt_id;

                $getView['store_name'] = $data['result'][0]->store_name;
                
                $getView['domain'] = $data['result'][0]->domain;

                echo json_encode($getView);

            }else{

                echo json_encode('error');

            }

            

    }

   

     public function tweet_preview(){

         $this->assets = array(

            'css' => $this->config->item('css'),

            'js' => $this->config->item('js'),

            'title' => 'Tweet Feed'

        );

        $getId = $_GET['id'];

        if($getId == 'undefined' || $getId == ''){

            $getView = array();

            ob_start();

            $this->load->view('display_follow_tweet_view');

            $output = ob_get_contents();

            ob_end_clean();

            $getView[] = $output;

            echo json_encode($getView);

        }

        else{

          $getView = array();

            ob_start();

            $this->db->select('*');

            $this->db->from('usersettings');

            $this->db->join('single_tweet_setting','usersettings.id = single_tweet_setting.store_id');

            $this->db->where('single_tweet_setting.crypt_id',$getId);

            $data['result'] = $this->db->get();



            if($data['result']->num_rows() > 0){

                $data['result'] = $data['result']->result();

                $this->load->view('display_follow_tweet_view', $data);

            }

            $output = ob_get_contents();

            ob_end_clean();

            $getView[] = $output;

            $getView['crypt_id'] = $data['result'][0]->crypt_id;

            echo json_encode($getView);

        }

    }



     public function single_embedded_tweet() {



        $this->assets = array(

            'css' => $this->config->item('css'),

            'js' => $this->config->item('js'),

            'title' => 'Single Tweet Feed'

        );

        $getId = explode(",", $_GET['ids']);

        $shop = $_GET['shop'];

        $getView = array();

            ob_start();

            $this->db->select('*');

            $this->db->from('usersettings');

            $this->db->join('single_embedded_tweet','usersettings.id = single_embedded_tweet.store_id');

            $this->db->where_in('single_embedded_tweet.crypt_id',$getId);

            //$this->db->where('usersettings.store_name', $shop);

            //$data['result'] = $this->db->get();
            $where = "usersettings.store_name ='".$shop."' OR usersettings.domain ='".$shop."'";
            $this->db->where($where);
            $data['result'] = $this->db->get();

            if($data['result']->num_rows() > 0){

                $data['result'] = $data['result']->result();

                $this->load->view('display_single_embedded_tweet', $data);

                $output = ob_get_contents();

                ob_end_clean();

                $getView[] = $output;

                $getView['crypt_id'] = $data['result'][0]->crypt_id;

                $getView['store_name'] = $data['result'][0]->store_name;
                
                $getView['domain'] = $data['result'][0]->domain;

                echo json_encode($getView);

            }else{

                echo json_encode('error');

            }

           

    }

     public function single_embedded_tweet_preview(){

         $this->assets = array(

            'css' => $this->config->item('css'),

            'js' => $this->config->item('js'),

            'title' => 'single_embedded_tweet preview'

        );

         $getId = $_GET['id'];

         if($getId == 'undefined' || $getId == ''){

            $getView = array();

            ob_start();

            $this->load->view('display_single_embedded_tweet');

            $output = ob_get_contents();

            ob_end_clean();

            $getView[] = $output;

            echo json_encode($getView);

        }else{

            $getView = array();

            ob_start();

            $this->db->select('*');

            $this->db->from('usersettings');

            $this->db->join('single_embedded_tweet','usersettings.id = single_embedded_tweet.store_id');

            $this->db->where('single_embedded_tweet.crypt_id',$getId);

            $data['result'] = $this->db->get();



            if($data['result']->num_rows() > 0){

                $data['result'] = $data['result']->result();

                $this->load->view('display_single_embedded_tweet', $data);

            }

            $output = ob_get_contents();

            ob_end_clean();

            $getView[] = $output;

            $getView['crypt_id'] = $data['result'][0]->crypt_id;

            echo json_encode($getView);
        }
    }
}

