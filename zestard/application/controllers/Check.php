<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Check extends CI_Controller {
       
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Tweet_Feed_Model');
    }

    public function index() {
        
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Tweet Feed'
        );
        $getId = explode(",", $_GET['ids']);
        $shop = $_GET['shop'];
        $getView = array();
            ob_start();
            $this->db->select('*');
            $this->db->from('usersettings');
            $this->db->join('tweet_feed_settings','usersettings.id = tweet_feed_settings.store_id');
            $this->db->where_in('tweet_feed_settings.crypt_id',$getId);
            //$this->db->where('usersettings.store_name', $shop);
            $where = "usersettings.store_name ='".$shop."' OR usersettings.domain ='".$shop."'";
            $this->db->where($where);
            $data['result'] = $this->db->get();
            
            if($data['result']->num_rows() > 0){
                $data['result'] = $data['result']->result();
                $this->load->view('display_tweet_feed', $data);
                $output = ob_get_contents();
                ob_end_clean();
                $getView[] = $output;
                $getView['crypt_id'] = $data['result'][0]->crypt_id;
                $getView['store_name'] = $data['result'][0]->store_name;
                $getView['domain'] = $data['result'][0]->domain;
                echo json_encode($getView);
            }else{
                echo json_encode('error');
            }
            

    }
    public function admin_preview()
    {
         $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Tweet Feed'
        );
        $getId = $_GET['id'];
         if($getId == 'undefined' || $getId == ''){
            $getView = array();
            ob_start();
            $this->load->view('display_tweet_feed');
            $output = ob_get_contents();
            ob_end_clean();
            $getView[] = $output;
            echo json_encode($getView);
        }else{
           $getView = array();
            ob_start();
            $this->db->select('*');
            $this->db->from('usersettings');
            $this->db->join('tweet_feed_settings','usersettings.id = tweet_feed_settings.store_id');
            $this->db->where('tweet_feed_settings.crypt_id',$getId);
            $data['result'] = $this->db->get();

            if($data['result']->num_rows() > 0){
                $data['result'] = $data['result']->result();
                $this->load->view('display_tweet_feed', $data);
            }
            $output = ob_get_contents();
            ob_end_clean();
            $getView[] = $output;
            $getView['crypt_id'] = $data['result'][0]->crypt_id;
            echo json_encode($getView);
        }
        

    }

    
}
