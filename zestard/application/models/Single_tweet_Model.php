<?php



class Single_tweet_Model extends CI_Model {



    function __construct() {

        parent::__construct();

    }



    public function insert($data) {

        if ($this->db->insert("single_tweet_setting", $data)) {



            /* Start Add For Crypt Code */

            $insert_id = $this->db->insert_id();

            $crypt_id = crypt($insert_id, 'ze');

            $finaly_encrypt = str_replace(['/','.'], "Z", $crypt_id);
            
            $this->db->set('crypt_id', $finaly_encrypt);

            $this->db->where('id', $insert_id);

            $this->db->update('single_tweet_setting');

            /* End For Crypt Code */

            return true;

        }

    }



    public function update($data, $id) {

        $this->db->set($data);

        $this->db->where("id", $id);

        $this->db->update("single_tweet_setting", $data);

    }



    public function update_data($data, $id) {

        $this->db->set($data);

    }





    public function single_embedded_tweet_insert($data) {

        if ($this->db->insert("single_embedded_tweet", $data)) {



            /* Start Add For Crypt Code */

            $insert_id = $this->db->insert_id();

            $crypt_id = crypt($insert_id, 'ze');

            $finaly_encrypt = str_replace(['/','.'], "Z", $crypt_id);
            
            $this->db->set('crypt_id', $finaly_encrypt);

            $this->db->where('id', $insert_id);

            $this->db->update('single_embedded_tweet');

            /* End For Crypt Code */

            return true;

        }

    }

    public function single_embedded_tweet_update($data, $id) {

        $this->db->set($data);

        $this->db->where("id", $id);

        $this->db->update("single_embedded_tweet", $data);

    }

     public function single_embedded_tweet_update_data($data, $id) {

        $this->db->set($data);

    }



    public function getStoreIdByStoreName($shop) {

        $query = $this->db->get_where("usersettings", array("store_name" => $shop));

        return $data['shopData'] = $query->result();

    }



    public function getStoreIdByStoreNameFromController($shop) {

        $query = $this->db->get_where("usersettings", array("store_name" => $shop));

        return $data['shopData'] = $query->result();

    }



}



?>