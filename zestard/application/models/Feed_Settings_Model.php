<?php 
class Feed_Settings_Model extends CI_Model {
    function __construct() {        
        parent::__construct();    
        
    }   
    public function insert($data) {        
        if ($this->db->insert("tweet_feed_settings", $data)) {
            /* Start Add For Crypt Code */            
            $insert_id = $this->db->insert_id();            
            $crypt_id = crypt($insert_id, 'ze');
            $finaly_encrypt = str_replace(['/','.'], "Z", $crypt_id);
            $this->db->set('crypt_id', $finaly_encrypt);            
            $this->db->where('id', $insert_id);            
            $this->db->update('tweet_feed_settings');
            /* End For Crypt Code */           
            return true;        
            
        }    
        
    }    
    public function update($data, $id) {        
        $this->db->set($data);        
        $this->db->where("id", $id);        
        $this->db->update("tweet_feed_settings", $data);    
        
    }    
    public function update_data($data, $id) {        
        $this->db->set($data);    
        
    }    
    public function getStoreIdByStoreName($shop) {        
        $query = $this->db->get_where("usersettings", array("store_name" => $shop));        
        return $data['shopData'] = $query->result();    
        
    }    
    public function getStoreIdByStoreNameFromController($shop) {        
        $query = $this->db->get_where("usersettings", array("store_name" => $shop));        
        return $data['shopData'] = $query->result();    
        
    }        
    public function getAppSettings($id) {        
        $query = $this->db->get_where("appsettings", array("id" => $id));        
        return $data['appdata'] = $query->result();    
        
    }
    
}
?>