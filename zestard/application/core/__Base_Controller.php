<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');
require __DIR__ . '/../../../vendor/shopifymodel/shopify_api/client.php';
require __DIR__ . '/../../../vendor/shopifymodel/wcurl/wcurl.php';

use shopifymodel\shopify_api;

class Base_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->database();
        $this->load->model('Charge_Model');
    }

    public function checkStatus() {
        
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Manage payment'
        );

        $data['data'] = $this->Charge_Model->getDataByShop($_SESSION['shop']);
        if ($this->input->post('accepted')) {

            $chargeId = $this->input->post('id');
            //API for activate the app
            $select_settings = $this->db->query("SELECT * FROM appsettings WHERE id = 1")->row();
            $query = $this->db->select('*');
            $query = $this->db->from('usersettings');
            $query = $this->db->where(array('store_name' => $_SESSION['shop']));
            $query = $this->db->get();
            $userData = $query->result();
            $API_KEY = $select_settings->api_key;
            $SECRET = $select_settings->shared_secret;
            $TOKEN = $userData[0]->access_token;
            $store_name = $userData[0]->store_name;
            $shopify = shopify_api\client(
                 $store_name, $TOKEN, $API_KEY, $SECRET
            );
            $getActivateResponse = $shopify('GET', '/admin/recurring_application_charges/'.$chargeId.'.json');
            if ($data['data'][0]->status == 'pending' && $getActivateResponse['status'] == 'accepted') {
                $getActivateResponse = $shopify('POST', '/admin/recurring_application_charges/'.$chargeId.'/activate.json');
                $data2['status'] = $getActivateResponse['status'];
                $this->db->where(array('charge_id' => $getActivateResponse['id'],'api_client_id' => $getActivateResponse['api_client_id']));
                $this->db->update('usersettings',$data2);
                return true;
             }else{
                 if ($data['data'][0]->status == 'pending' && ($getActivateResponse['status'] != 'accepted' || $getActivateResponse['status'] != 'active')) {
                    $output['errorMsg'] = "Sorry Your App is Decline please Reinstall app";
                    $this->load->view('themepart/charge_head', $this->assets);
                    $this->load->view('activation_error', $output);
                 }
             }

    } else {
            if ($data['data'][0]->status == 'pending') {
                $this->load->view('themepart/charge_head', $this->assets);
                $this->load->view('paymentpage', $data);die();
            }
            return false;
        }

        // @TODO: Remove the following if not required
        session_write_close();
    }
}
