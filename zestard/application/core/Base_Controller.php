<?php
session_start();
class Base_Controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();		$this->load->database();		$this->load->model('Feed_Settings_Model');		
        if(empty($_SESSION))
			$_SESSION['shop'] = $_GET['shop'];
		
		$getApiKey = $this->Feed_Settings_Model->getAppSettings(1);
		$_SESSION['api_key'] = $getApiKey[0]->api_key;
	}
}