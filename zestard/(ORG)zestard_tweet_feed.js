jQuery(document).ready(function () {
    //var ids = jQuery('.zestard-tweetfeed').attr('id');
    /*
     * normal feed ajax
     */
    var ids = jQuery.map(jQuery('.zestard-tweetfeed'),function(n,i){
        return jQuery(n).attr('id');
    });
    if(ids.length > 0){
        jQuery.ajax({
        url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check?ids=' + ids + '&shop=' + document.domain,
        dataType: "json",
        cache: false,
        method: "get",
        success: function (res) {
            if(res == 'error'){
                jQuery(".zestard-tweetfeed").html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store did not match with current store '+document.domain+'</div>');
            }else{
                var id;
                jQuery(".zestard-tweetfeed").each(function(){
                    id = jQuery(this).attr('id');
                    if(id == res.crypt_id && (document.domain == res.store_name || document.domain == res.domain)){
                        jQuery(this).html(res[0]);
                    }else{
                        jQuery(this).html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store did not match with current store '+document.domain+'</div>');
                    }
                });
            }
        },
        error: function (xhr, status, err) {
            jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                    .appendTo(document.body);
        }
    });

    }
    
    /*
     * single feed ajax
     */
    var ids1 = jQuery.map(jQuery('.zestard-single-tweetfeed'),function(n,i){
        return jQuery(n).attr('id');
    });
     if(ids1.length > 0){
         jQuery.ajax({
        url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check_single_tweet?ids=' + ids1 + '&shop=' + document.domain,
        dataType: "json",
        cache: false,
        method: "get",
        success: function (res) {
             if(res == 'error'){
                jQuery(".zestard-single-tweetfeed").html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store did not match with current store '+document.domain+'</div>');
            }else{
                var id;
                jQuery(".zestard-single-tweetfeed").each(function(){
                    id = jQuery(this).attr('id');
                    if(id == res.crypt_id && (document.domain == res.store_name || document.domain == res.domain)){
                        jQuery(this).html(res[0]);
                    }else{
                        jQuery(this).html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store did not match with current store '+document.domain+'</div>');
                    }
                });
            }
        },
        error: function (xhr, status, err) {
            jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                    .appendTo(document.body);
        }
    });
     }
    
    /*
     * embedded single feed ajax
     */
    var ids2 = jQuery.map(jQuery('.zestard-single-embedded-tweet'),function(n,i){
        return jQuery(n).attr('id');
    });
    
    if(ids2.length > 0){
        jQuery.ajax({
          url: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/Check_single_tweet/single_embedded_tweet?ids=' + ids2 + '&shop=' + document.domain,
        dataType: "json",
        method: "get",
        success: function (res) {
            if(res == 'error'){
                jQuery(".zestard-single-embedded-tweet").html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store did not match with current store '+document.domain+'</div>');
            }else{
                var id;
                jQuery(".zestard-single-embedded-tweet").each(function(){
                    id = jQuery(this).attr('id');
                    if(id == res.crypt_id && document.domain == res.store_name){
                        jQuery(this).html(res[0]);
                    }else{
                        jQuery(this).html('<div class="alert alert-danger"> <strong>Alert!</strong> Oops! Your store did not match with current store '+document.domain+'</div>');
                    }
                });
            }
        },
        error: function (xhr, status, err) {
            jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                    .appendTo(document.body);
        }
    });
    }
    
});