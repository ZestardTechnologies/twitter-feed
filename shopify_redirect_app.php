<?php

session_start(); //start a session
require 'vendor/autoload.php';
//require 'vendor/autoload.php';
//require 'vendor/autoload.php';

use shopifymodel\shopify_api;

$db = new Mysqli("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_twitter_feed");
if ($db->connect_errno) {
    die('Connect Error: ' . $db->connect_errno);
}
$select_settings = $db->query("SELECT * FROM appsettings WHERE id = 1");
$app_settings = $select_settings->fetch_object();

if (!empty($_GET['shop']) && !empty($_GET['code'])) {
    $shop = $_GET['shop']; //shop name
    //get permanent access token
    $access_token = shopify_api\oauth_access_token(
                    $_GET['shop'], $app_settings->api_key, $app_settings->shared_secret, $_GET['code']
    );
    
    
    
    //webhook for unistall app

    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
    $params = array("webhook" => array("topic" => "app/uninstalled",
            "address" => "https://www.zestardshop.com/shopifyapp/twitterfeed/hook.php",
            "format" => "json"));
    
    $appUninstallHook = shopify_api\appUninstallHook(
                    $access_token, $url, $params
    );
    
    $shopifydomain = shopify_api\client(
                   $_GET['shop'], $access_token,$app_settings->api_key, $app_settings->shared_secret
    );
    $getshopresponse = array();
    $getshopresponse = $shopifydomain('GET', '/admin/shop.json');
    $domain = $getshopresponse['domain'];
    
    //echo '<pre>';print_r($getshopresponse);die;
    
    //add scriptag
	$scriptData = array(
		'script_tag' => array(
			'event' => 'onload', 
			'src' 	=> 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/zestard_tweet_feed.js'
		)
	);
	$scriptTag = $shopifydomain('POST', '/admin/script_tags.json',$scriptData);
    
    $db->query("
    INSERT INTO usersettings 
    SET access_token = '$access_token',
    store_name = '$shop',
    domain = '$shop'
 ");
    
    //follow up mail
    $follow_up_headers = 'From: Zestard Technologies <shilpi@zestard.com>'. "\r\n";
    $follow_up_headers .= 'MIME-Version: 1.0' . "\r\n";
    $follow_up_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $subject ="Zestard Installation Greetings :: Twitter Feed";
    $app_name = "Twitter Feed";
    $logo = 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/assets/images/zestard-logo.png';
    $installation_follow_up_msg ='<html>

                    <head>
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <style>
                            @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i");
                            @media only screen and (max-width:599px) {
                                table {
                                    width: 100% !important;
                                }
                            }
                            
                            @media only screen and (max-width:412px) {
                                h2 {
                                    font-size: 20px;
                                }
                                p {
                                    font-size: 13px;
                                }
                                .easy-donation-icon img {
                                    width: 120px;
                                }
                            }
                        </style>
                    
                    </head>
                    
                    <body style="background: #f4f4f4; padding-top: 57px; padding-bottom: 57px;">
                        <table class="main" border="0" cellspacing="0" cellpadding="0" width="600px" align="center" style="border: 1px solid #e6e6e6; background:#fff; ">
                            <tbody>
                                <tr>
                                    <td style="padding: 30px 30px 10px 30px;" class="review-content">
                                        <p class="text-align:left;"><img src="'.$logo.'" alt=""></p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;"><b>Hi '.$getshopresponse["shop_owner"].'</b>,</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Thanks for Installing Zestard Application '.$app_name.'</p>
                                        <p style="font-family: \'Helvetica\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We appreciate your kin interest for choosing our application and hope that you have a wonderful experience.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Please don\'t feel hesitate to reach us in case of any queries or questions at <a href="mailto:support@zestard.com" style="text-decoration: none;color: #1f98ea;font-weight: 600;">support@zestard.com</a>.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We also do have live chat support services for quick response and resolution of queries.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">(Please Note: Support services are available according to the IST Time Zone(i.e GMT 5:30+) as we reside in India. Timings are from 10:00am to 7:00pm)</p>
                    
                                    </td>
                                </tr>
                    
                                <tr>
                                    <td style="padding: 20px 30px 30px 30px;">
                    
                                        <br>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 26px; margin-bottom:0px;">Thanks,<br>Zestard Support</p>
                                    </td>
                                </tr>
                    
                            </tbody>
                        </table>
                    </body>';

    //mail($getshopresponse['email'], $subject, $installation_follow_up_msg, $follow_up_headers);

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $msg = '<table>
                            <tr>
                                <th>Shop Name</th>
                                <td>' . $getshopresponse['name'] . '</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>' . $getshopresponse['email'] . '</td>
                            </tr>
                            <tr>
                                <th>Domain</th>
                                <td>' . $getshopresponse['domain'] . '</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>' . $getshopresponse['phone'] . '</td>
                            </tr>
                            <tr>
                                <th>Shop Owner</th>
                                <td>' . $getshopresponse['shop_owner'] . '</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>' . $getshopresponse['country_name'] . '</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>' . $getshopresponse['plan_name'] . '</td>
                            </tr>
                          </table>';
    //Geting Development store from the database
    $dev_store = $db->query("SELECT * FROM development_stores WHERE dev_store_name = '$shop'");
    if($dev_store->num_rows <= 0){
        mail("support@zestard.com", "Twitter Feed App Installed", $msg, $headers);
        
    }
    else{
        //mail("chandraprakash.zestard@gmail.com", "Twitter Feed App Installed", $msg, $headers);
    }
    //save the signature and shop name to the current session
    $_SESSION['shopify_signature'] = $_GET['signature'];
    $_SESSION['shop'] = $shop;
    $_SESSION['api_key'] = $app_settings->api_key;

    header('Location: https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/');
   // header('Location: ' . $charge["confirmation_url"] . '');
}
?>
